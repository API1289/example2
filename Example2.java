public class Example2{

	public static void armstrongNumber(int n){
		String s = String.valueOf(n);
		int sum = 0;
		for(int i = 0 ; i < s.length() ; i++){
			sum += Math.pow(Integer.parseInt(String.valueOf(s.charAt(i))), s.length());
		}
		if(String.valueOf(sum).equals(s)){
			System.out.println(s + " is an Armstrong number");
		}
		else{
			System.out.println(s + " is not an Armstrong number");
		}
	}

    	public static void main(String[] args){
        	int a = 10;
        	armstrongNumber(a);
    	}
}
